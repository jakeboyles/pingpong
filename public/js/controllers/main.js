angular.module('communityController', ['angularMoment','infinite-scroll'])


// inject the Todo service factory into our controller
.controller('mainController', function($scope,$rootScope,$location, $http, Users) {
	$scope.formData = {};
	$scope.loading = true;
	var posts = [];
	var number = 0;
	$scope.posts = [];
	var page = {number:number};
	var noMore = false;
	var users;

	$scope.send = function(id,email) {
		alert(email);
		$(".fa-spinner").show();
		var info = {sender:$scope.player, user:id, email:email}; 

		Users.sendUser(info)
		.success(function(data) {
			$(".fa-spinner").hide();
			alert("SENT!");
		});
	}


	$scope.randomGame = function() {
		var length = users.members.length;
		var one = Math.floor((Math.random() * length) + 1); 
		var two = Math.floor((Math.random() * length) + 1); 
		
		one = users.members[one];
		two = users.members[two];

		Users.randomGame(one,two)
		.success(function(data) {
		$('.fa-spinner').hide();
	});
	}


	Users.get()
	.success(function(data) {
		users = data;
		$scope.users=data;
		$('.fa-spinner').hide();
	});


});

