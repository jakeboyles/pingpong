angular.module('scotchTodo', ['communityController','SearchController','ProfileController','SinglePostController','SigninController','PostController','SignupController', 'communityService','ngRoute'])

.config(function($routeProvider) {
$routeProvider
    .when('/', {
    controller:'mainController',
    templateUrl:'views/list.html'
    })
    .otherwise({
    redirectTo:'/'
    });
})

