angular.module('communityService', [])
// super simple service
// each function returns a promise object 
.factory('Users', function($http) {
return {
	get : function(page) {
	return $http.get('/api/users',page);
	},
	sendUser : function(user) {
	return $http.post('/api/send', {user:user});
	},
	randomGame : function(user,usertwo) {
	return $http.post('/api/randomGame', {user1:user,usertwo:usertwo});
	}
}
});